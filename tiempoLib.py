
import numpy as np

#ECUACIÓN DEL TIEMPO
def ecuacion_tiempo(dia):           
    
    B = 2*np.pi*(dia - 1)/365
    E = 229.5*(75 + 1868*np.cos(B) - 32077*np.sin(B) - 14615*np.cos(2*B) - 40890*np.sin(2*B))*1E-6

    return E

#CÁLCULO DEL GMTincidence_angle(self, sigma, omega, beta, gamma)
GMT = lambda hs, long, dia: hs + long/15 - ecuacion_tiempo(dia)/60

#LONGITUD DE REFERENCIA PARA ESPAÑA
def long_referencia(dia):

    if dia >= 87 and dia < 304:
        L_ref = -30 #GMT+2
        
    else:
        L_ref = -15 #GMT+1    
        
    return L_ref

#HORA LOCAL EN FUNCIÓN DE LA LONG. DE REFERENCIA Y HORA SOLAR(>0 HACIA EL OE)
hora_local = lambda hs, dia, long: hs + (long - long_referencia(dia))/15 - ecuacion_tiempo(dia)/60

#duracion de los meses
dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 0]

#HORAS ACUMULADAS
def horas_acumuladas(mes, dia, hora):
    
    hora_acum = 0        
    for mes in range(-1, mes):
            
        hora_acum += dias_mes[mes]
        
    hora_acum += dia - 1
    hora_acum *= 24
    hora_acum += hora
        
    return hora_acum

#DIAS ACUMULAD0S
def dias_acumulados(mes, dia):
    
    dias_acum = 0        
    for mes in range(-1, mes):
            
        dias_acum += dias_mes[mes]
        
    dias_acum += dia - 1
        
    return dias_acum