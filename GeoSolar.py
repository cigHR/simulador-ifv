import numpy as np
import json
from scipy.interpolate import InterpolatedUnivariateSpline
import tiempoLib as tl
import optimizacionLib as opt

class Horizonte:
    """ Definición de la clase <Horizonte>

    Esta clase tiene por objeto cargar los datos relativos al perfil de un 
    horizonte, caracterizado por una nube de puntos Altura[º] - Acimut[º],
    así como la determinación de su influencia para unas coordenadas solares
    particulares

    Atributos
    ---------
        - altura_horizonte_arr : list
            lista que contiene la altura (en grados) de los puntos que definen
            al perfil del horizonte
        
        - acimut_horizonte_arr : list
            lista que contiene el acimut (en grados) de los puntos que definen
            al perfil del horizonte

        - altura_horizonte : func
            función de interpolación lineal de la nube de puntos 2D dada por 
            las listas <altura_horizonte_arr> y <acimut_horizonte_arr>  

    """

    def __init__(self, fichero_horizonte: str) -> None: 
        """ Inicializador de la clase <Horizonte>
        
        Se importa la nube de puntos que define al horizonte de un .json,
        para confeccionar <altura_horizonte_arr> y <acimut_horizonte_arr>  
        así como la función de interpolación <acimut_horizonte>

        Parámetros
        ----------
            - fichero_horizonte : str
                nombre del fichero + extensión (json) que contiene al
                perfil del horizonte

        Salidas
        -------
            - : None 
        """

        self.altura_horizonte_arr = []
        self.acimut_horizonte_arr = []
        self.influencia_horizonte = function

        with open(fichero_horizonte) as data_fichero:
            data =  json.load(data_fichero)
            num_data = len(data['outputs']['horizon_profile']) #nº de puntos

            #confección de las listas y conversión a radianes
            for i in range(0, num_data):   
                self.altura_horizonte_arr.append(
                    data['outputs']['horizon_profile'][i]['H_hor'] * np.pi / 180
                    )
                self.acimut_horizonte_arr.append(
                    data['outputs']['horizon_profile'][i]['A'] * np.pi / 180
                    )

            self.altura_horizonte = InterpolatedUnivariateSpline(
                self.acimut_horizonte_arr, 
                self.altura_horizonte_arr, 
                k=1
                )

    def influencia_horizonte(
        self, 
        altura_solar: float, 
        acimut_solar: float
        ) -> bool:
        """ Método <influencia_horizonte>

        Esta función determina si el disco solar queda obstaculizado por el
        perfil del horizonte

        Atributos
        ---------
            - altura solar : float
                altura del disco solar
            
            - acimut solar : float
                acimut del disco solar

        Salidas
        -------
            - : bool
                Devuelve True si el disco solar es obstaculizado, ya sea por 
                el horizonte o por el plano horizontal local. De lo contrario,
                devuelve False
        """
        
        if altura_solar <= 0:
            return False
        else:
            altura_horizonte = self.altura_horizonte(acimut_solar)
            if altura_horizonte > altura_solar:
                return True
            else:
                return False


class GeometriaSolar:
    """Definición de la clase <GeometriaSolar>

    Esta clase reúne los métodos necesarios para el cálculo de las coordenadas
    solares, en función de la hora solar, para un determinado emplazamiento, 
    especificado por una latitud y una longitud. Asimismo, almacena los valores
    horarios/diarios de ciertas variables de geometría solar que resultan útiles
    en balances de energía sobre un plano de captación.

    Atributos
    ---------
        - latitud : float
            latitud del emplazamiento (en radianes)
        
        - longitud : float
            longitud del emplazamiento

        - declinacion_arr : list
            lista cuyos elementos son la declinación terrestre para cada día
            del año (en radianes)
        
        - cos_cenital_arr : list
            lista cuyos elementos son el coseno del ángulo cenital para cada
            hora del año

        - ang_solar_arr : 
    """
    

    def __init__(self, fichero_climatico, fichero_horizonte = None):
        
        #importación de datos climáticos y localización
        self.fichero_climatico = fichero_climatico
        self.fichero_horizonte = fichero_horizonte
        self.carga_datos_climaticos()

        #datos de geometría solar
        self.declinacion_arr = [None for i in range(365)] #declinacion terrestre [rad]
        self.cos_cenital_arr = [None for i in range(365 * 24)] #coseno del ángulo cenital [rad]
        self.influencia_horizonte_arr = [False for i in range(365 * 24)] #acimut solar [rad]
        self.ang_solar_arr = [None for i in range(24)] #ángulo solar [rad]

        #generación de datos
        self.genera_datos()
        

    #IMPORTACIÓN DE DATOS HORARIOS
    def carga_datos_climaticos(self):
        
        with open(self.fichero_climatico) as data_fichero:
    
            data =  json.load(data_fichero)
            num_data = len(data['outputs']['tmy_hourly'])
            
            self.Gglobal = []
            self.Gdifusa = []
            self.Tbar = []
            
            for i in range(0, num_data):   
                self.Gglobal.append(data['outputs']['tmy_hourly'][i]['G(h)'])
                self.Gdifusa.append(data['outputs']['tmy_hourly'][i]['Gd(h)'])
                self.Tbar.append(data['outputs']['tmy_hourly'][i]['T2m'])

        #TEMPERATURAS EXTREMAS
        self.T_min = min(self.Tbar)
        self.T_max = max(self.Tbar)

        #LOCALIZACIÓN
        self.phi = data['inputs']['location']['latitude']
        self.phi_rad = np.pi * self.phi / 180
        self.long = data['inputs']['location']['longitude']
        self.elevacion = data['inputs']['location']['elevation']

 
    #DECLINACIÓN
    declinacion = lambda self, n_dia: (np.pi / 180) * 23.45 * np.sin(2 * np.pi * (284 + n_dia) / 365)
    
    #ÁNGULO SOLAR
    ang_solar = lambda self, hs: (np.pi / 12 ) * (hs - 12)
    
    #COSENO DEL ÁNGULO CENITAL
    cos_cenital = lambda self, sigma, omega: np.sin(sigma)*np.sin(self.phi_rad) + np.cos(sigma)*np.cos(self.phi_rad)*np.cos(omega)

    #ACIMUT SOLAR
    def acimut_solar(self, sigma, omega, cos_cenital):

        arg = (cos_cenital * np.sin(self.phi_rad) - np.sin(sigma)) / (np.sin(np.arccos(cos_cenital)) * np.cos(self.phi_rad))
        acimut_s = (2 * (omega >= 0) - 1) * abs(np.arccos(min(0.999, max(-0.999, arg))))

        return acimut_s

    #ALTURA SOLAR
    altura_solar = lambda self, cos_cenit: np.pi / 2 - np.arccos(cos_cenit)

    #CÁLCULO Y ALMACENAMIENTO DE LA DECLINACIÓN, HORA SOLAR Y ÁNGULO CENITAL
    def genera_datos(self):

        for dia in range(365):
            self.declinacion_arr[dia] = self.declinacion(dia)
        
        for hora_solar in range(24):
            self.ang_solar_arr[hora_solar] = self.ang_solar(hora_solar)

        hora_acum = 0
        for dia in range(365):
            for hora_solar in range(24):
                self.cos_cenital_arr[hora_acum] = self.cos_cenital(self.declinacion_arr[dia], 
                                                                self.ang_solar_arr[hora_solar])
                hora_acum += 1
        
        if self.fichero_horizonte is not None:
            horizon = Horizonte(self.fichero_horizonte)
            hora_acum = 0
            for dia in range(365):
                for hora_solar in range(24):
                    acimut_solar = self.acimut_solar(self.declinacion_arr[dia], 
                                                    self.ang_solar_arr[hora_solar], 
                                                    self.cos_cenital_arr[hora_solar])
                    altura_solar = self.altura_solar(self.cos_cenital_arr[hora_acum])
                    self.influencia_horizonte_arr[hora_acum] = horizon.influencia_horizonte(altura_solar, 
                                                                                            acimut_solar)
                    hora_acum += 1

    #EXPORTACIÓN DE DATOS
    def exporta_datos(self):

        datos = {
            'Gg': self.Gglobal,
            'Gd': self.Gdifusa,
            'phi': self.phi,
            'angulo solar': self.ang_solar_arr,
            'declinacion': self.declinacion_arr,
            'cos cenital': self.cos_cenital_arr,
            'horizonte': self.influencia_horizonte_arr
            }
        
        return datos
        

class ReceptorSolar:
    
    #PLANO UNITARIO DE CAPTACIÓN DE LA RADIACIÓN SOLAR
    def __init__(self, beta, gamma, recursoSolar):

        #datos importados de geometria solar
        self.importa_datos(recursoSolar)

        #inclinación y orientación del plano
        self.beta_rad = beta * np.pi / 180 #ángulo de inclinación del plano [rad]
        self.gamma_rad = gamma * np.pi / 180 #ángulo de orientación del plano [rad]
        self.phi_rad = self.phi * np.pi / 180 #latitud [rad]

        #factores de visión simplificados
        self.fv_suelo = (1 - np.cos(self.beta_rad)) / 2 
        self.fv_cielo = (1 + np.cos(self.beta_rad)) / 2

        #datos de radiación sobre el plano
        self.albedo = 0.2 #albedo [-]
        self.irradiacion_arr = [] #irradiación horaria anual del plano [wh / m2]

    #IMPORTACIÓN DE DATOS
    def importa_datos(self, recursoSolar):
        
        self.Gglobal = recursoSolar['Gg']
        self.Gdifusa = recursoSolar['Gd']
        self.phi = recursoSolar['phi']
        self.ang_solar_arr = recursoSolar['angulo solar']
        self.declinacion_arr = recursoSolar['declinacion']
        self.cos_cenital_arr = recursoSolar['cos cenital']
        self.influencia_horizonte_arr = recursoSolar['horizonte']

    #ÁNGULO DE INCIDENCIA CON RESPECTO A LA NORMAL UN PLANO CON INCLINACIÓN <<BETA>> Y ORIENTACIÓN <<GAMMA>>
    def cos_incidencia(self, sigma, omega):
        
        s1 = np.sin(sigma) * np.sin(self.phi_rad) * np.cos(self.beta_rad)
        s2 = -np.sin(sigma) * np.cos(self.phi_rad) * np.sin(self.beta_rad) * np.cos(self.gamma_rad)
        s3 = np.cos(sigma) * np.cos(self.phi_rad) * np.cos(self.beta_rad) * np.cos(omega)
        s4 = np.cos(sigma) * np.sin(self.phi_rad) * np.sin(self.beta_rad) * np.cos(self.gamma_rad) * np.cos(omega)
        s5 = np.cos(sigma) * np.sin(self.beta_rad) * np.sin(self.gamma_rad) * np.sin(omega)
        
        return s1 + s2 + s3 + s4 + s5


    #CÁLCULO DEL FACTOR DE CONVERSIÓN GEOMÉTRICO HORARIO PARA EL MES ESPECIFICADO
    def genera_rb_mensual(self, mes):

        rb_arr = []
        hora_acum = tl.horas_acumuladas(mes, 1, 0)
        dias_acum = tl.dias_acumulados(mes, 1)
        
        for dia in range(0, tl.dias_mes[mes], 1):
            for hora in range(24):
                
                if hora < 4 or hora > 22:
                    rb_arr.append(0)

                else:
                    cos_ang_inc = self.cos_incidencia(self.declinacion_arr[dia + dias_acum], self.ang_solar_arr[hora])
                    rb_arr.append(cos_ang_inc/self.cos_cenital_arr[hora_acum])

                hora_acum += 1

        return rb_arr

    #MODELO DE RADIACIÓN SOBRE SUPERFICIE INCLINADA DE LIU & JORDAN
    def Liu_Jordan(self, Gglobal, Gdifusa, rb, horizonte):

        if rb <= 0 or Gglobal <= 0: 
            return 0

        Gdir = rb * (Gglobal - Gdifusa) * horizonte

        if Gdir > 1367:
            return 0

        Gdif = Gdifusa * self.fv_cielo
        Gref = self.albedo * Gglobal * self.fv_suelo
        Gtotal = Gdir + Gdif + Gref

        return Gtotal
    
    #CÁLCULO DE LA IRRADIACIÓN HORARIA DEL PLANO PARA EL MES ESPECIFICADO
    def genera_irradiacion_mensual(self, mes):

        irradiacion_arr = []
        hora_acum_glob = tl.horas_acumuladas(mes, 1, 0)
        hora_acum_mes = 0
        rb_arr = self.genera_rb_mensual(mes)
        
        for dia in range(0, tl.dias_mes[mes], 1):
            for hora in range(24):
                sin_horizonte =  1 - self.influencia_horizonte_arr[hora_acum_glob]
                irradiacion_arr.append(self.Liu_Jordan(self.Gglobal[hora_acum_glob], self.Gdifusa[hora_acum_glob], 
                                                        rb_arr[hora_acum_mes]))
                hora_acum_glob += 1
                hora_acum_mes += 1

        return irradiacion_arr
        
    #CÁLCULO Y ALMACENAMIENTO DEL FACTOR DE CONVERSIÓN GEOMÉTRICO Y DE LA IRRADIACIÓN SOBRE EL PLANO
    def genera_datos(self):
        
        for mes in range(12):
            self.irradiacion_arr += self.genera_irradiacion_mensual(mes)
  
    #CÁLCULO DEL ÁNGULO QUE OPTIMIZA LA IRRADIACIÓN EN LOS MESES ESPECIFICADOS
    def angulo_optimo(self, meses_arr, resetea = False):

        beta_rad_original = self.beta_rad
        
        def funcion_objetivo(beta_rad):

            self.beta_rad = beta_rad
            irradiacion_acum = 0

            for mes in meses_arr:
                irradiacion_acum += sum(self.genera_irradiacion_mensual(mes))

            #print(self.beta_rad * 180 / np.pi, irradiacion_acum / 1000) 
            return -irradiacion_acum

        beta_rad_opt = opt.min_metodo_aureo(funcion_objetivo, 0, np.pi / 2, tau = 0.01)

        if resetea:
            self.beta_rad = beta_rad_opt
            self.beta = self.beta_rad * 180 / np.pi
            self.irradiacion_arr = []
            self.genera_datos()

        else:
            self.beta_rad = beta_rad_original
        
        return beta_rad_opt * 180 / np.pi


gs = GeometriaSolar('tmy_ejemplo.json', 'horizon_ejemplo.json')
#plano = ReceptorSolar(0, 0, gs.exporta_datos())

count = 0
for i in range(len(gs.influencia_horizonte_arr)):
    if gs.influencia_horizonte_arr[i]:
        count += 1

print(count)



#print(plano.angulo_optimo([i for i in range(12)]))




    
    





    
    

    

    



    

    

